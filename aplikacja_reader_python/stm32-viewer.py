# pip install pyserial
# pip install matplotlib

import re
import time
import threading

import tkinter as tk
from typing import List, Any

import serial
import serial.tools.list_ports

from tkinter import ttk, Tk
from matplotlib.backends.backend_tkagg import (FigureCanvas)
from matplotlib.figure import Figure

def uart_read_thread(s):
    # serial data read
    serial_string_line = s.readline().decode('utf-8')
    if serial_string_line[0:3] == "adc":
        # decode data from printed serial line - use https://regex101.com/ to parse
        regex = r"\d*\.\d+|\d+"
        matches = re.finditer(regex, serial_string_line)
        for matchNum, match in enumerate(matches, start=1):
            if matchNum == 1:
                x_int = int(match[0])
            if matchNum == 2:
                y_float = float(match[0])
                # x.append(x_int)
                y.append(y_float)
                x = range(len(y))
                data.plot(x, y)
                canvas.draw()
    threading.Timer(0.01, uart_read_thread(s)).start()


def _quit():
    root.quit()  # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent

def _clear_data():
    data.clear()
    canvas.draw()
    x.clear()
    y.clear()

def _read_data(thread):    
    global s
    if not s.isOpen():
        s.port=combobox1.get()
        s.open()    
        # start read from serial port in seperated thred
        thread.start()
        
    print("Read serial port")
    # send data request to stm32
    command = 'start ' + comboList.get() + '\r'
    print(command)
    s.write(command.encode())

x = []
y = []

root: Tk = tk.Tk()
root.wm_title("STM32 data viewer")
root.geometry ("600x400")

fig = Figure(figsize=(5, 2), dpi=80)
data = fig.add_subplot(111)
data.set_title('ADC chart[V]')

canvas = FigureCanvas(fig, master=root)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

# assign serial 
s = serial.Serial()
s.baudrate=38400
# assign read deamon thread
thread = threading.Thread(target=uart_read_thread, args=(s,))
thread.setDaemon(True)

# add button to canvas
button_read = tk.Button(master=root, text="STM32 Read Data", command=lambda: _read_data(thread))
button_quit = tk.Button(master=root, text="QUIT", command=_quit)
button_clear = tk.Button(master=root, text="DATA CLEAR", command=_clear_data)

# add combobox with available port
combobox1 = ttk.Combobox(root, width=6)
comboList = ttk.Combobox(root , values = ["10" , "20", "30", "40", "50"], width=3)
comboList.set("10")

# positioning
combobox1.pack(side=tk.LEFT)
comboList.pack(side=tk.LEFT)

button_read.pack(side=tk.LEFT)
button_clear.pack(side=tk.LEFT)
button_quit.pack(side=tk.RIGHT)

# find available port
ports = serial.tools.list_ports.comports()
list_cb: List[Any] = []
for port, desc, hwid in sorted(ports):
    list_cb.append(port)
    print("{}: {} [{}]".format(port, desc, hwid))


# add ports name to combobox list
combobox1['values'] = list_cb
combobox1.set(port)

tk.mainloop()
