/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "stream_buffer.h"
#include "timers.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct {
	const char* cmdSt;
	void(*cmdCb)(uint32_t param);
} cmdTab_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define ADC_DMA_BUFFER_SIZE 50
#define UART_RX_BUFFER_SIZE 20
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define EnableInterrupts() asm ("CPSIE  i")
/* Macro to disable all interrupts. */
#define DisableInterrupts() asm ("CPSID  i")

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim15;

UART_HandleTypeDef huart2;

/* Definitions for controlTask */
osThreadId_t controlTaskHandle;
const osThreadAttr_t controlTask_attributes = {
  .name = "controlTask",
  .stack_size = 150 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* USER CODE BEGIN PV */

StreamBufferHandle_t xStreamBuffer;

// timer 15 counter
volatile uint8_t timer15Cnt = 0;

// dam buffer data counter
volatile uint32_t dmaBufferCnt = 0;

// DMA ADC buffer
uint16_t  adcDataBuffer[ADC_DMA_BUFFER_SIZE];

// ADC conversion trigger timer handler
TimerHandle_t  adcDMATriggerTimer;

// uart rx data buffer - null terminating string buffer
char UartRxDataBuff[UART_RX_BUFFER_SIZE] = {0};

// size of dma data request
uint32_t dmaRequestSize;

// stream buffer
const size_t xStreamBufferSizeBytes = sizeof(adcDataBuffer);
const size_t xTriggerLevel = sizeof(adcDataBuffer[0]);

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM15_Init(void);
static void MX_TIM3_Init(void);
void StartControlTask(void *argument);

/* USER CODE BEGIN PFP */
void cmdStartCb(uint32_t param);
void cmdSetFreqCb(uint32_t freq);
void cmdSetPwmCb(uint32_t procent);

static const cmdTab_t cmdTab[] = {
	{	.cmdSt = "start",	.cmdCb = cmdStartCb,	},
	{	.cmdSt = "freq",	.cmdCb = cmdSetFreqCb,	},
	{	.cmdSt = "pwm",		.cmdCb = cmdSetPwmCb,	},
};

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static void sendDataToTerminal(char const *st, ...)
{

	char buff[60];

	va_list arg;
	va_start(arg, st);
	vsprintf(buff, st, arg);
    va_end(arg);

	if (HAL_OK != HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), 20))
	{
		// software breakpoint
		__asm__("BKPT");
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	static uint8_t buttonPressCnt = 0;
	sendDataToTerminal("Button pressed: %d\r\n", buttonPressCnt++);
}

void timer_Callback(struct __TIM_HandleTypeDef *htim)
{
	if (htim == &htim15)
	{
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		sendDataToTerminal("Timer 15 elapsed:%d\r", timer15Cnt++);
		// should disable due to set timer 15 event - IT mode only
	//	HAL_ADC_Start_IT(&hadc);
	}
	else if (htim == &htim3)
	{
//		// if PWM on channel 1 is greater than timer period should be zero
//		if (htim->Instance->CCR2++ > htim->Instance->ARR)
//		{
//			htim->Instance->CCR2 = 0;
//		}
	}
}

void adcComplete_Callback(struct __ADC_HandleTypeDef *hadc)
{
	// Stop timer 15
	HAL_TIM_Base_Stop(&htim15);
	// reset timer 15 counter
	timer15Cnt = 0;

	// ADC 12 bit (1) = 3,3 V - ADC IRQ Mode - use line below only in IRQ mode
//	uint16_t adcData = HAL_ADC_GetValue(hadc);


	BaseType_t xHigherPriorityTaskWoken = pdFALSE; /* Initialised to pdFALSE. */

	/* Attempt to send the string to the stream buffer. */
	size_t xBytesSent = xStreamBufferSendFromISR( xStreamBuffer,
										   ( void * ) adcDataBuffer,
										   dmaRequestSize * sizeof(adcDataBuffer[0]),
										   &xHigherPriorityTaskWoken );

	if( xBytesSent != dmaRequestSize * sizeof(adcDataBuffer[0]) )
	{
		/* There was not enough free space in the stream buffer for the entire
		string to be written, ut xBytesSent bytes were written. */
		// software breakpoint
		__asm__("BKPT");
	}
	/* If xHigherPriorityTaskWoken was set to pdTRUE inside
	xStreamBufferSendFromISR() then a task that has a priority above the
	priority of the currently executing task was unblocked and a context
	switch should be performed to ensure the ISR returns to the unblocked
	task.  In most FreeRTOS ports this is done by simply passing
	xHigherPriorityTaskWoken into taskYIELD_FROM_ISR(), which will test the
	variables value, and perform the context switch if necessary.  Check the
	documentation for the port in use for port specific instructions. */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

void vDMATriggerTimerCallback(TimerHandle_t timer)
{
	// reset dma buffer counter
	dmaBufferCnt = 0;
	dmaRequestSize = sizeof(adcDataBuffer) / sizeof(adcDataBuffer[0]);
	// Start timer 15 - which triggers every single adc conversion in the dma process - without interrupts
	HAL_TIM_Base_Start(&htim15);
	// Start ADC DMA mode process
	HAL_ADC_Start_DMA(&hadc, (uint32_t*) adcDataBuffer, dmaRequestSize);
}

void uartReceive_Callback(UART_HandleTypeDef *huart)
{
	static char *pUartRxData = UartRxDataBuff;

	if (*pUartRxData++ == '\r' || strlen((char *)UartRxDataBuff) == (UART_RX_BUFFER_SIZE - 1))
	{
//		TODO: improvement
//		A better solution would be to send the string to the queue
//		and parse it in a separate task. Due to the lack of RAM memory,
//		a queueless solution was used

		// command dispatcher
		for(uint8_t i = 0; i < (sizeof(cmdTab) / sizeof(cmdTab[0])); i++)
		{
			if (memcmp(cmdTab[i].cmdSt, UartRxDataBuff, strlen(cmdTab[i].cmdSt)) == 0)
			{
				// param parsing
				uint32_t param = atoi((char *)UartRxDataBuff + strlen(cmdTab[i].cmdSt));
				// command matched - if not NULL call command callback
				if (cmdTab[i].cmdCb)
					cmdTab[i].cmdCb(param);
			}
		}
		//	clear buffer and set read pointer at buffer first element
		memset(UartRxDataBuff, 0, UART_RX_BUFFER_SIZE);
		pUartRxData = UartRxDataBuff;
	}
	// re-arm rx interrupt
	HAL_UART_Receive_IT(huart, (uint8_t *)pUartRxData, 1);
}

void cmdStartCb(uint32_t param)
{
	sendDataToTerminal("Start: %d\r\n", param);
	// buffer size check
	if (param > sizeof(adcDataBuffer) / sizeof(adcDataBuffer[0]))
	{
		sendDataToTerminal("requested buffer size too large\r\n", param);
		return;
	}
	// Start DMA adc
	// reset dma buffer counter
	dmaBufferCnt = 0;
	// Start timer 15 - which triggers every single adc conversion in the dma process - without interrupts
	HAL_TIM_Base_Start_IT(&htim15);
	// Start ADC DMA mode process
	dmaRequestSize = param;
	HAL_ADC_Start_DMA(&hadc, (uint32_t*) adcDataBuffer, param);
}

void cmdSetFreqCb(uint32_t freq)
{
	sendDataToTerminal("Freq: %d\r\n", freq);

//	TODO: Assignment for students !!!
//	Implement a change in the sampling frequency
//	of the ADC measurement taken from the freq parameter
}

void cmdSetPwmCb(uint32_t procent)
{
	sendDataToTerminal("pwm: %d\r\n", procent);

	if (procent > htim3.Instance->ARR)
	{
		sendDataToTerminal("requested pwm value too large\r\n", procent);
		return;
	}

	htim3.Instance->CCR2 = procent;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM15_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  MX_DMA_Init();
  MX_ADC_Init();

  // Registering callback for timer 15 interrupt
  HAL_TIM_RegisterCallback(&htim15, HAL_TIM_PERIOD_ELAPSED_CB_ID, timer_Callback);

  // Registering callback for timer 3 interrupt
  HAL_TIM_RegisterCallback(&htim3, HAL_TIM_PWM_PULSE_FINISHED_CB_ID, timer_Callback);

  // Registering callback for adc complete interrupt
  HAL_ADC_RegisterCallback(&hadc, HAL_ADC_CONVERSION_COMPLETE_CB_ID, adcComplete_Callback);

  // Registering callback for uart rx complete interrupt
  HAL_UART_RegisterCallback(&huart2, HAL_UART_RX_COMPLETE_CB_ID , uartReceive_Callback);
  HAL_UART_Receive_IT(&huart2, (uint8_t *)UartRxDataBuff, 1);

  // start toggle pin mode output compare channel 1
  HAL_TIM_OC_Start_IT(&htim3, TIM_CHANNEL_1);
  // start PWM timer 3 on channel 2
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  // Trigger DMA timer
  /* start timers, add new ones, ... */
  adcDMATriggerTimer = xTimerCreate("Trigger DMA", 2000, pdTRUE, ( void * ) 0, vDMATriggerTimerCallback);
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */

  /* Create a stream buffer that can hold xStreamBufferSizeBytes bytes.
   	 The memory used to hold both the stream buffer structure and the data in the stream buffer is
     allocated dynamically.
  */
  xStreamBuffer = xStreamBufferCreate( xStreamBufferSizeBytes, xTriggerLevel );

  if( xStreamBuffer == NULL )
  {
	  /* There was not enough heap memory space available to create the
	  stream buffer. */
	  __asm__("BKPT");
  }
  else
  {
	  /* The stream buffer was created successfully and can now be used. */
  }
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of controlTask */
  controlTaskHandle = osThreadNew(StartControlTask, NULL, &controlTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T15_TRGO;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 48000 - 1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 100 - 1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 10;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM15 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM15_Init(void)
{

  /* USER CODE BEGIN TIM15_Init 0 */

  /* USER CODE END TIM15_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM15_Init 1 */

  /* USER CODE END TIM15_Init 1 */
  htim15.Instance = TIM15;
  htim15.Init.Prescaler = 48000 - 1;
  htim15.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim15.Init.Period = 100 - 1;
  htim15.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim15.Init.RepetitionCounter = 0;
  htim15.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim15) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim15, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim15, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM15_Init 2 */

  /* USER CODE END TIM15_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartControlTask */
/**
  * @brief  Function implementing the controlTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartControlTask */
void StartControlTask(void *argument)
{
  /* USER CODE BEGIN 5 */
	sendDataToTerminal("Control task started\r\n");

	// Start timer 15 and enable its interrupt
//	HAL_TIM_Base_Start_IT(&htim15);

	// Enable ADC complete interrupt - IT mode
//	HAL_ADC_Start_IT(&hadc);

	// Start ADC DMA trigger timer
//	xTimerStart(adcDMATriggerTimer, 2000);

	/* Infinite loop */
	for(;;)
	{
		uint16_t adcData;
		size_t xReceivedBytes;

		/* Receive up to another sizeof(adcData) bytes from the stream buffer.
		Wait in the Blocked state (so not using any CPU processing time) for a
		maximum of 100ms for the full sizeof( ucRxData ) number of bytes to be
		available. */
		xReceivedBytes = xStreamBufferReceive( xStreamBuffer,
											   ( void * ) &adcData,
											   sizeof( adcData ),
											   portMAX_DELAY );

		if( xReceivedBytes > 0 )
		{
			/* A adcData contains another xRecievedBytes bytes of data, which can
			be processed here.... */
			float voltageFloat = ((float)adcData / 0x0FFF) * 3.3;
			uint8_t voltageDec = voltageFloat;
			uint8_t voltagePrec = (voltageFloat - voltageDec) * 100;
			sendDataToTerminal("adc[%d]:%d.%02d[V]\r\n",
								dmaBufferCnt++, voltageDec, voltagePrec);
		}

		osDelay(10);
	}
  /* USER CODE END 5 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM17 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM17) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
